package com.example.reactive;

import com.example.reactive.repositories.ContactsRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EntityScan("com.example.reactive.models")
@ComponentScan(basePackages = "com.example.reactive")
@EnableReactiveMongoRepositories(basePackageClasses = ContactsRepository.class)
@SpringBootApplication()
public class ReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveApplication.class, args);
    }

}
