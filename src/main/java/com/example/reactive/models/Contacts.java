package com.example.reactive.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
@Data
public class Contacts implements Serializable {
    @Id
    String id;
    String name;
    String lastName;
    String email;
    String phone;
    String address;
    String age;

}
