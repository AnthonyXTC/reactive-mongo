package com.example.reactive.controllers;

import com.example.reactive.models.Contacts;
import com.example.reactive.repositories.ContactsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
public class ContactsController {
    @Autowired
    private ContactsRepository contactsRepository;


    @GetMapping("/contacts")
    public Flux<Contacts> getAllContacts() {
        return contactsRepository.findAll();
    }

    @PostMapping("/contact")
    public Mono<Contacts> createContact(@Valid @RequestBody Contacts contacts) {
        return contactsRepository.save(contacts);
    }

    @GetMapping("/contact/{id}")
    public Mono<ResponseEntity<Contacts>> getContactById(@PathVariable(value = "id") String id) {
        return contactsRepository.findById(id)
                .map(contacts -> ResponseEntity.ok(contacts))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/contact/{id}")
    public Mono<ResponseEntity<Void>> deleteContact(@PathVariable(value = "id") String id) {
        return contactsRepository.findById(id)
                .flatMap(contacts -> contactsRepository.deleteById(id)
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


}
