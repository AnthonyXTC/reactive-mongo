package com.example.reactive.repositories;

import com.example.reactive.models.Contacts;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ContactsRepository extends ReactiveMongoRepository<Contacts, String> {
//    Mono<Contacts> findByPhoneAndAddress(String phone, String address);
}
